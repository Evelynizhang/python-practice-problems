# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    special = ["$", "!", "@"]
    def has_lower(password):
        for each in password:
            if each.islower() == True:
                return True
    def has_upper(password):
        for each in password:
            if each.isupper() == True:
                return True
    def has_digit(password):
        for each in password:
            if each.isdigit() == True:
                return True
    def has_special(password):
        for each in password:
            for s in special:
                if each == s:
                    return True

    if has_lower(password) == True and has_upper(password) == True and has_digit(password) == True and has_special(password) == True and len(password) >= 6 and len(password) <= 12:
        return True
    else:
        return False

print(check_password('687d!aA6df'))
