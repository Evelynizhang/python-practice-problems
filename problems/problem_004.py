# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def max_of_three(value1, value2, value3):
    list_of_value = []
    list_of_value.append(value1)
    list_of_value.append(value2)
    list_of_value.append(value3)
    max = list_of_value[0]
    for temp_max in list_of_value:
        if max >= temp_max:
            max = max
        else:
            max = temp_max
    return max

print(max_of_three(1, 2, 3))
print(max_of_three(4, 2, 3))
print(max_of_three(3, 2, 3))
print(max_of_three(1, 5, 3))
