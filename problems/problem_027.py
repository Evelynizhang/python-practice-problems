# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) == 0:
        return None
    else:
        max = values[0]
        for each in values:
            if each >= max:
                max = each
        return max

print(max_in_list([10,2,4,3,6,5,8,4]))
