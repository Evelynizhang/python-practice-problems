# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    if values == None:
        return None
    else:
        sum = 0
        for each in values:
            sum = sum + each
        return sum / len(values)

print(calculate_average([1,2,3,4,5]))
print(calculate_average([1,2,3,4,50]))
